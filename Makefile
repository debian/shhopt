# $Id: Makefile,v 1.12 2002/03/02 19:57:25 sverrehu Exp $
DIST		= shhopt
VERMAJ		= 1
VERMIN		= 1
VERPAT		= 7
VERSION		= $(VERMAJ).$(VERMIN).$(VERPAT)

# Define SHARED as 1 for Linux shared ELF library
#SHARED		= 1

ifeq ($(SHARED),1)
LIBTARGET	= lib$(DIST).so.$(VERSION)
LIBTARGETSO	= lib$(DIST).so
LIBTARGETSOMAJ	= $(LIBTARGETSO).$(VERMAJ)
CCSHRD		= -fPIC
else
LIBTARGET	= lib$(DIST).a
endif

LIBHEAD		= $(DIST).h
TARGETS		= $(LIBTARGET)

INSTBASEDIR	= /usr/local
INSTLIBDIR	= $(INSTBASEDIR)/lib
INSTINCDIR	= $(INSTBASEDIR)/include
INSTALL		= install -m 644
INSTALLPROG	= install -m 755
MKDIRP		= install -d -m 755

CC		= gcc
OPTIM		= -O2

INCDIR		= -I.

CCOPT		= -s -Wall $(OPTIM) $(INCDIR)

# Object files to store in the library
LIBOBJS		= shhopt.o


all: $(TARGETS)

# don't worry if you get ranlib not found errors.  This probably means
# that your ar does an implicit ranlib and you do not need to run ranlib
# separately.  This error is harmless.
$(LIBTARGET): $(LIBOBJS)
ifeq ($(SHARED),1)
	$(CC) -shared -Wl,-soname,$(LIBTARGETSOMAJ) -o $(LIBTARGET) $(LIBOBJS)
else
	ar rc $(LIBTARGET) $(LIBOBJS)
	ranlib $(LIBTARGET) || true
endif

# Note that you may need GNU's -liberty if your libc lacks strtoul
example: $(LIBTARGET) example.o
	$(CC) -o example example.c \
		-L. -I. -L$(INSTLIBDIR) -I$(INSTINCDIR) -lshhopt

.c.o:
	$(CC) $(CCSHRD) -o $@ -c $(CCOPT) $<

depend dep:
	$(CC) $(INCDIR) -MM *.c >depend

install: $(LIBTARGET)
	$(MKDIRP) $(INSTLIBDIR) $(INSTINCDIR)
	$(INSTALL) $(LIBTARGET) $(INSTLIBDIR)
	$(INSTALL) $(LIBHEAD) $(INSTINCDIR)
ifeq ($(SHARED),1)
	ln -sf $(LIBTARGET) $(INSTLIBDIR)/$(LIBTARGETSOMAJ)
	ln -sf $(LIBTARGETSOMAJ) $(INSTLIBDIR)/$(LIBTARGETSO)
	echo "Now run ldconfig if necessary."
endif

clean:
	rm -f *.o core *~ depend

chmod:
	chmod a+r *

# To let the author make a distribution. The rest of the Makefile
# should be used by the author only.
LSMFILE		= $(DIST)-$(VERSION).lsm
DISTDIR		= $(DIST)-$(VERSION)
DISTFILE	= $(DIST)-$(VERSION).tar.gz
DISTFILES	= README INSTALL CREDITS ChangeLog TODO \
		  Makefile $(LSMFILE) \
		  $(LIBHEAD) \
		  shhopt.c \
		  example.c

$(LSMFILE): $(DIST).lsm.in
	VER=$(VERSION); \
	DATE=`date "+%d%b%y"|tr '[a-z]' '[A-Z]'`; \
	sed -e "s/VER/$$VER/g;s/DATE/$$DATE/g" $(DIST).lsm.in > $(LSMFILE)

veryclean: clean
	rm -f $(TARGETS) $(DISTFILE) $(LSMFILE) example

dist: $(LSMFILE) chmod
	mkdir $(DISTDIR)
	chmod a+rx $(DISTDIR)
	ln $(DISTFILES) $(DISTDIR)
	tar -cvzf $(DISTFILE) $(DISTDIR)
	chmod a+r $(DISTFILE)
	rm -rf $(DISTDIR)

ifeq (depend,$(wildcard depend))
include depend
endif
